const express = require("express");
const os = require("os");
const cluster = require("cluster");

const app = express();

const numCPUs = os.cpus().length;

app.get("/", (req, res) => {
  res.send(`process id => ${process.pid}`);
});

app.get("/kill", (req, res) => {
  cluster.worker.kill();
});

if (cluster.isMaster) {
  console.log(`Master server started on ${process.pid}`);
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }
  cluster.on("exit", (worker, code, signal) => {
    console.log(`Worker ${worker.process.pid} died. Restarting`);
    cluster.fork();
  });
} else {
  console.log(`Cluster server started on ${process.pid}`);
  app.listen(5000, () => console.log("server started on port 5000"));
}
